ARGoS Project
======
The project aims at completing a ARGoS robot project, which consists of installing
ARGoS, writing some robot control code, and run some simulations.

## Tasks:
1. Download the examples from
(http://www.argos-sim.info/examples.php) and
modify the "_footbot_diffusion_" controller, which simulates the behaviour of a gas particle.
2. The challenge is to find a different strategy (e.g. change direction as soon as an obstacle is detected) for obstacle avoidance, and to implement it in the controller.

## Verification:
We will use the file "_experiments/diffusion_10.argos_" to test your code and see what you have implemented.

## Procedure:

### Platform Setup and Testing:
1. The Ubuntu WLS and the window-server xMing are installed. To configure the window-server following:

> echo "export DISPLAY=:0" >> ~/.bashrc  
> source ~/.bashrc

2. The ARGoS package is installed by using the following command to install all the dependencies.
> sudo apt install -f ./argos3_simulator-3.0.0-x86_64-beta49.deb

3. Clone the git repository:
> git clone https://github.com/ilpincy/argos3-examples.git argos3-examples

4. Compile, build and run a sample code:
> $ mkdir build  
> $ cd build  
> $ cmake -DCMAKE_BUILD_TYPE=Debug ..  
> $ make  
> $ cd ..  
> $ argos3 -c experiments/diffusion_1.argos

### Original Code Understanding:
1. Obtain the readings from the proximity sensor, which has 23 readings from different direction of the robot as demonstrated in the following figure.

![Proximity Sensor](https://bitbucket.org/Jay_ZJ_Lin/argos3-project/raw/master//images/pic1.png)

2. By averaging the proximity sensor readings, the distance and direction of the obstacle can be obtained.
3. The algorithm is:
* If the angle is within [-10,10] and the distance is smaller than the threshold: move forward;
* Else if angle is larger than 0: Turn right; if angle is smaller than 0: Turn left.

### To Implement a New Algorithm:
New algorithm considers to manipulate the speed of left and right wheels with the contacting angle and distance.
* if there is no contacting: the robot moves forward
* if there is contacting with angle $\angle C$ and Distance $\Delta D$, the speed of left and right wheels are decided by
> $V_{left} = V sin(\gamma)$  
> $V_{right} = V sin(\gamma)$  
> $\gamma = \frac{\pi}{2} - \frac{\angle C}{4}  (D_{thresh} - \Delta D)$   {if $\angle C >= 0$}  
> $\gamma =  - \frac{\angle C}{4}  (D_{thresh} - \Delta D)$   {if $\angle C < 0$}  


The angles are mapped to $[-\pi/4, 3\pi/4]$ range and is controlled by the CDelta. The concept is
* if the CDelta is approaching the threshold m_fDelta: The angle is mapped to the $[0,\pi/2]$ region.
* if the CDelta reaches the threshold m_fDelta: The angle is mapped to $\pi/2$ .
* if the CDelta is over the threshold m_fDelta: The angle is mapped to the $[\pi/2,3\pi/4]$ and $[-\pi/4,0]$ region.

The code block is demonstrated below:

```C
void CFootBotDiffusion::ControlStep()
{
    /* Get readings from proximity sensor */
    const CCI_FootBotProximitySensor::TReadings &tProxReads = m_pcProximity->GetReadings();
    /* Sum them together */
    CVector2 cAccumulator;
    for (size_t i = 0; i < tProxReads.size(); ++i)
    {
        cAccumulator += CVector2(tProxReads[i].Value, tProxReads[i].Angle);
    }
    cAccumulator /= tProxReads.size();
    /*
    New algorithm considers to manipulate the speed of left and right wheels with the contacting angle and distance.
    * if there is no contacting: the robot moves forward
    * if there is contacting with angle "CAngle" and Distance "CDelta", the speed of left and right wheels are decided by
    * left = Velocity * sin(GammaAngle)
    * right = Velocity * cos(GammaAngle)
    * GammaAngle = PI / 2 - CAngle / 4 * (m_fDelta - CDelta)   -- CAngle >= 0
    * GammaAngle = GammaAngle = -CAngle / 4 * (m_fDelta - CDelta)   -- CAngle < 0
    * 
    * The angles are mapped to [-PI/4, 3PI/4] range and is controlled by the CDelta. The concept is
    * if the CDelta is approaching the threshold m_fDelta: The angle is mapped to the [0,PI/2] region.
    * if the CDelta reaches the threshold m_fDelta: The angle is mapped to PI/2 .
    * if the CDelta is over the threshold m_fDelta: The angle is mapped to the [PI/2,3PI/4] and [-PI/4,0] region.
    * 
    * [ISSUE] The knowing issue of the method is that, if a robot enters into the bottom right conor with a pillar right next to the walls, 
    * the robot will be stucked at this conor and never can leave.
    * [ANALYSIS] The reason is when the robot detecting obstacles from more than one direction, the calculated obstacle direction is the averaged
    * direction, the robot will try to turn and circle around it. This eventually results in the robot circles inside the small region.
    * [SOLUTION] Differentiate different obstacles.
   */
    CRadians CAngle = cAccumulator.Angle(); //Contacting Angle
    CRadians GammaAngle;                    //Steering Angle
    Real CDelta = cAccumulator.Length();    //Contacting Distance
    CVector2 VelocityTrue;                  //The left and right speed of the robot wheels

    if (CDelta == 0) // If no contacting, move forward
    {
        VelocityTrue = CVector2(m_fWheelVelocity, m_fWheelVelocity);
    }
    else // If contacting, use the previous equation to decide the speed of wheels
    {
        if (CAngle.GetValue() >= 0)
        {
            GammaAngle = CRadians::PI / 2 - CAngle / 4 * (m_fDelta - CDelta);
        }
        else
        {
            GammaAngle = -CAngle / 4 * (m_fDelta - CDelta);
        }

        VelocityTrue = m_fWheelVelocity * CVector2(sin(GammaAngle.GetValue()), cos(GammaAngle.GetValue()));
    }
    m_pcWheels->SetLinearVelocity(VelocityTrue.GetX(), VelocityTrue.GetY()); //Write to the robot Differential Steering Actuator
}
```
### Simulation Results:
![Sim1](https://bitbucket.org/Jay_ZJ_Lin/argos3-project/raw/master//images/pic2.png)
![Sim2](https://bitbucket.org/Jay_ZJ_Lin/argos3-project/raw/master//images/pic3.png)
### Issue with the current vector navigation algorithm:
[ISSUE] The knowing issue of the method is that, if a robot enters into the bottom right conor with a pillar right next to the walls, the robot will be stucked at this conor and never can leave.

![ISSUE](https://bitbucket.org/Jay_ZJ_Lin/argos3-project/raw/master//images/pic4.png)

[ANALYSIS] The reason is when the robot detecting obstacles from more than one direction, the calculated obstacle direction is the averaged direction, the robot will try to turn and circle around it. This eventually results in the robot circles inside the small region.

[SOLUTION] Differentiate different obstacles.