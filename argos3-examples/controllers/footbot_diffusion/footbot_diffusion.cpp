/* Include the controller definition */
#include "footbot_diffusion.h"
/* Function definitions for XML parsing */
#include <argos3/core/utility/configuration/argos_configuration.h>
/* 2D vector definition */
#include <argos3/core/utility/math/vector2.h>

/****************************************/
/****************************************/

CFootBotDiffusion::CFootBotDiffusion() : m_pcWheels(NULL),
                                         m_pcProximity(NULL),
                                         m_cAlpha(10.0f),
                                         m_fDelta(0.1f),
                                         m_fWheelVelocity(2.5f),
                                         m_cGoStraightAngleRange(-ToRadians(m_cAlpha),
                                                                 ToRadians(m_cAlpha)) {}

/****************************************/
/****************************************/

void CFootBotDiffusion::Init(TConfigurationNode &t_node)
{
    /*
    * Get sensor/actuator handles
    *
    * The passed string (ex. "differential_steering") corresponds to the
    * XML tag of the device whose handle we want to have. For a list of
    * allowed values, type at the command prompt:
    *
    * $ 
    *
    * to have a list of all the possible actuators, or
    *
    * $ argos3 -q sensors
    *
    * to have a list of all the possible sensors.
    *
    * NOTE: ARGoS creates and initializes actuators and sensors
    * internally, on the basis of the lists provided the configuration
    * file at the <controllers><footbot_diffusion><actuators> and
    * <controllers><footbot_diffusion><sensors> sections. If you forgot to
    * list a device in the XML and then you request it here, an error
    * occurs.
    */
    m_pcWheels = GetActuator<CCI_DifferentialSteeringActuator>("differential_steering");
    m_pcProximity = GetSensor<CCI_FootBotProximitySensor>("footbot_proximity");
    /*
    * Parse the configuration file
    *
    * The user defines this part. Here, the algorithm accepts three
    * parameters and it's nice to put them in the config file so we don't
    * have to recompile if we want to try other settings.
    */
    GetNodeAttributeOrDefault(t_node, "alpha", m_cAlpha, m_cAlpha);
    m_cGoStraightAngleRange.Set(-ToRadians(m_cAlpha), ToRadians(m_cAlpha));
    GetNodeAttributeOrDefault(t_node, "delta", m_fDelta, m_fDelta);
    GetNodeAttributeOrDefault(t_node, "velocity", m_fWheelVelocity, m_fWheelVelocity);
}

/****************************************/
/****************************************/

void CFootBotDiffusion::ControlStep()
{
    /* Get readings from proximity sensor */
    const CCI_FootBotProximitySensor::TReadings &tProxReads = m_pcProximity->GetReadings();
    /* Sum them together */
    CVector2 cAccumulator;
    for (size_t i = 0; i < tProxReads.size(); ++i)
    {
        cAccumulator += CVector2(tProxReads[i].Value, tProxReads[i].Angle);
    }
    cAccumulator /= tProxReads.size();
    /*
    New algorithm considers to manipulate the speed of left and right wheels with the contacting angle and distance.
    * if there is no contacting: the robot moves forward
    * if there is contacting with angle "CAngle" and Distance "CDelta", the speed of left and right wheels are decided by
    * left = Velocity * sin(GammaAngle)
    * right = Velocity * cos(GammaAngle)
    * GammaAngle = PI / 2 - CAngle / 4 * (m_fDelta - CDelta)   -- CAngle >= 0
    * GammaAngle = GammaAngle = -CAngle / 4 * (m_fDelta - CDelta)   -- CAngle < 0
    * 
    * The angles are mapped to [-PI/4, 3PI/4] range and is controlled by the CDelta. The concept is
    * if the CDelta is approaching the threshold m_fDelta: The angle is mapped to the [0,PI/2] region.
    * if the CDelta reaches the threshold m_fDelta: The angle is mapped to PI/2 .
    * if the CDelta is over the threshold m_fDelta: The angle is mapped to the [PI/2,3PI/4] and [-PI/4,0] region.
    * 
    * [ISSUE] The knowing issue of the method is that, if a robot enters into the bottom right conor with a pillar right next to the walls, 
    * the robot will be stucked at this conor and never can leave.
    * [ANALYSIS] The reason is when the robot detecting obstacles from more than one direction, the calculated obstacle direction is the averaged
    * direction, the robot will try to turn and circle around it. This eventually results in the robot circles inside the small region.
    * [SOLUTION] Differentiate different obstacles.
   */
    CRadians CAngle = cAccumulator.Angle(); //Contacting Angle
    CRadians GammaAngle;                    //Steering Angle
    Real CDelta = cAccumulator.Length();    //Contacting Distance
    CVector2 VelocityTrue;                  //The left and right speed of the robot wheels

    if (CDelta == 0) // If no contacting, move forward
    {
        VelocityTrue = CVector2(m_fWheelVelocity, m_fWheelVelocity);
    }
    else // If contacting, use the previous equation to decide the speed of wheels
    {
        if (CAngle.GetValue() >= 0)
        {
            GammaAngle = CRadians::PI / 2 - CAngle / 4 * (m_fDelta - CDelta);
        }
        else
        {
            GammaAngle = -CAngle / 4 * (m_fDelta - CDelta);
        }

        VelocityTrue = m_fWheelVelocity * CVector2(sin(GammaAngle.GetValue()), cos(GammaAngle.GetValue()));
    }
    m_pcWheels->SetLinearVelocity(VelocityTrue.GetX(), VelocityTrue.GetY()); //Write to the robot Differential Steering Actuator
}

/****************************************/
/****************************************/

/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as
 * second argument.
 * The string is then usable in the configuration file to refer to this
 * controller.
 * When ARGoS reads that string in the configuration file, it knows which
 * controller class to instantiate.
 * See also the configuration files for an example of how this is used.
 */
REGISTER_CONTROLLER(CFootBotDiffusion, "footbot_diffusion_controller")
